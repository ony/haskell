# Copyright 2013 Alex Elsayed <eternaleye@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require hackage [ ghc_dep='>=7.6.0' has_lib=false has_bin=true ]

SUMMARY="git-annex allows managing files with git, without checking the
file contents into git."
DESCRIPTION="
git-annex allows managing files with git, without checking the file
contents into git. While that may seem paradoxical, it is useful when
dealing with files larger than git can currently easily handle, whether
due to limitations in memory, checksumming time, or disk space.

Even without file content tracking, being able to manage files
with git, move files around and delete files with versioned directory
trees, and use branches and distributed clones, are all very handy
reasons to use git. And annexed files can co-exist in the same git
repository with regularly versioned files, which is convenient for
maintaining documents, Makefiles, etc that are associated with
annexed files but that benefit from full revision control.
"
HOMEPAGE="http://git-annex.branchable.com/"

# Some image resources (doc/logo* */favicon.ico
# standalone/osx/git-annex.app/Contents/Resources/git-annex.icns)
# have a non-standard license which reads:
# "Free to modify and redistribute with due credit, and obviously free to use."
LICENCES="GPL-3 AGPL-3 || (  LGPL-2.1  LGPL-3 )"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    assistant [[ description = [ Build a dropbox-like frontend to git-annex ] ]]
    dbus [[ description = [ Control the assistant over dbus ] ]]
    doc [[ description = [ Install tons of extra docs (design, walk-throughs, etc.) ] ]]
    pairing [[ requires = [ webapp ] description = [ Pair repositories via the webapp ] ]]
    s3 [[ description = [ Amazon S3 special remote ] ]]
    webapp [[ requires = [ assistant ] description = [ Build a web-based interface for the assistant ] ]]
    webdav [[ description = [ WebDAV share special remote ] ]]
"

DEPENDENCIES="
    build+run:

    $(haskell_bin_dependencies "
        dev-haskell/IfElse
        dev-haskell/MissingH
        dev-haskell/QuickCheck[>=2.1]
        dev-haskell/SafeSemaphore
        dev-haskell/aeson
        dev-haskell/async
        dev-haskell/bloomfilter
        dev-haskell/byteable
        dev-haskell/bytestring
        dev-haskell/case-insensitive
        dev-haskell/concurrent-output[>=1.6]
        dev-haskell/containers[>=0.5.0.0]
        dev-haskell/cryptonite
        dev-haskell/crypto-api
        dev-haskell/data-default
        dev-haskell/directory
        dev-haskell/dlist
        dev-haskell/edit-distance
        dev-haskell/esqueleto
        dev-haskell/exceptions[>=0.6]
        dev-haskell/feed
        dev-haskell/filepath
        dev-haskell/free
        dev-haskell/hslogger
        dev-haskell/http-client
        dev-haskell/http-conduit
        dev-haskell/http-types
        dev-haskell/magic
        dev-haskell/monad-control
        dev-haskell/monad-logger
        dev-haskell/mtl[>=2]
        dev-haskell/network-uri[>=2.6]
        dev-haskell/network[>=2.6]
        dev-haskell/old-locale
        dev-haskell/optparse-applicative[>=0.11.0]
        dev-haskell/persistent
        dev-haskell/persistent-sqlite
        dev-haskell/persistent-template
        dev-haskell/process
        dev-haskell/random
        dev-haskell/regex-tdfa
        dev-haskell/resourcet
        dev-haskell/sandi
        dev-haskell/securemem
        dev-haskell/socks
        dev-haskell/stm-chans
        dev-haskell/stm[>=2.3]
        dev-haskell/text
        dev-haskell/time
        dev-haskell/torrent[>=10000.0.0]
        dev-haskell/transformers
        dev-haskell/unix
        dev-haskell/unix-compat
        dev-haskell/unordered-containers
        dev-haskell/utf8-string
        dev-haskell/uuid[>=1.2.6]
    ")
    s3? (
        $(haskell_bin_dependencies "
            dev-haskell/aws[>=0.9.2]
            dev-haskell/conduit
            dev-haskell/conduit-extra
        ")
    )
    webdav? (
        $(haskell_bin_dependencies "
            dev-haskell/DAV[>=1.0]
        ")
    )
    assistant? (
        run:
            sys-process/lsof
        $(haskell_bin_dependencies "
            dev-haskell/dns
            dev-haskell/hinotify
            dev-haskell/mountpoints
        ")
    )
    dbus? (
        $(haskell_bin_dependencies "
            dev-haskell/dbus[>=0.10.7]
            dev-haskell/fdo-notify[>=0.3]
        ")
    )
    webapp? (
        $(haskell_bin_dependencies "
            dev-haskell/blaze-builder
            dev-haskell/clientsession
            dev-haskell/path-pieces[>=0.1.4]
            dev-haskell/shakespeare[>=2.0.0]
            dev-haskell/template-haskell
            dev-haskell/wai
            dev-haskell/wai-extra
            dev-haskell/warp-tls[>=1.4]
            dev-haskell/warp[>=3.0.0.5]
            dev-haskell/yesod-core[>=1.2.19]
            dev-haskell/yesod-default[>=1.2.0]
            dev-haskell/yesod-form[>=1.3.15]
            dev-haskell/yesod-static[>=1.2.4]
            dev-haskell/yesod[>=1.2.6]
        ")
    )
    pairing? (
        $(haskell_bin_dependencies "
            dev-haskell/network-info
            dev-haskell/network-multicast
        ")
    )
    doc? ( app-text/ikiwiki )
"

# enable (or make option) if wanted by someone: TestSuite
CABAL_SRC_CONFIGURE_PARAMS=(
    "--flags=ConcurrentOutput"
    "--flags=Cryptonite"
    "--flags=MagicMime"
    "--flags=Production"
    "--flags=TorrentParser"
    "--flags=-TestSuite"
)
CABAL_SRC_CONFIGURE_OPTION_FLAGS=(
    "assistant Assistant"
    "dbus Dbus"
    "pairing Pairing"
    "s3 S3"
    "webapp Webapp"
    "webdav WebDAV"
)

src_install() {
    cabal_src_install

    # Install git-annex-shell.
    dosym /usr/$(exhost --target)/bin/git-annex \
        /usr/$(exhost --target)/bin/git-annex-shell

    # FIXME Install the man pages. Requires ikiwiki.
    #emake -j1 DESTDIR="${IMAGE}" install-mans

    if option doc ; then
        emake -j1 DESTDIR="${IMAGE}" install-docs
    fi
}

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-fix-crypto-api-dep.patch
    "${FILES}"/${PNV}-655f707990b4144ac82244c19188af0e15bff546.patch
)

