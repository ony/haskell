# Copyright 2011 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require hackage

SUMMARY="Terminal GUI library in the niche of ncurses"
DESCRIPTION="
vty is a terminal interface library.

Vty currently provides:
* Automatic handling of window resizes.
* Supports Unicode characters on output, automatically setting and
  resetting UTF-8 mode for xterm. Other terminals are assumed to support
* Efficient output.
* Minimizes repaint area, thus virtually eliminating the flicker
  problem that plagues ncurses programs.
* A pure, compositional interface for efficiently constructing display
  images.
* Automatically decodes keyboard keys into (key,[modifier]) tuples.
* Automatically supports refresh on Ctrl-L.
* Automatically supports timeout after 50ms for lone ESC (a barely
  noticable delay)
* Interface is designed for relatively easy compatible extension.
* Supports all ANSI SGR-modes (defined in console_codes(4)) with
  a type-safe interface.
* Properly handles cleanup.

Current disadvantages:
* The character encoding of the output terminal is assumed to be UTF-8.
* Minimal support for special keys on terminals other than the
  linux-console.  (F1-5 and arrow keys should work, but anything
  shifted isn't likely to.)
* Uses the TIOCGWINSZ ioctl to find the current window size, which
  appears to be limited to Linux and *BSD.
"
HOMEPAGE="http://trac.haskell.org/vty/"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    $(haskell_lib_dependencies "
        dev-haskell/blaze-builder[>=0.3.3.2&<0.5]
        dev-haskell/bytestring
        dev-haskell/containers
        dev-haskell/data-default[>=0.5.3]
        dev-haskell/deepseq[>=1.1&<1.5]
        dev-haskell/directory
        dev-haskell/filepath[>=1.0&<2.0]
        dev-haskell/hashable[>1.2]
        dev-haskell/lens[>=3.9.0.2&<5.0]
        dev-haskell/mtl[>=1.1.1.0&<2.3]
        dev-haskell/parallel[>=2.2&<3.3]
        dev-haskell/parsec[>=2&<4]
        dev-haskell/terminfo[>=0.3&<0.5]
        dev-haskell/text[>=0.11.3]
        dev-haskell/transformers[>=0.3.0.0]
        dev-haskell/unix
        dev-haskell/utf8-string[>=0.3&<1.1]
        dev-haskell/vector[>=0.7]
    ")
    $(haskell_test_dependencies "
        dev-haskell/quickcheck-assertions[>=0.1.1]
        dev-haskell/smallcheck[>=1]
        dev-haskell/string-qq
        dev-haskell/test-framework-smallcheck[>=0.2]
    ")
"

BUGS_TO="alip@exherbo.org"

