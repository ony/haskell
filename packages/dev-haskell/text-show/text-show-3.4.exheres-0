# Copyright 2016 Marc-Antoine Perennou <keruspe@exherbo.org>
# Copyright 2017 Markus Rothe <mrothe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require hackage

SUMMARY="Efficient conversion of values into Text"
DESCRIPTION="
text-show offers a replacement for the Show typeclass intended
for use with Text instead of Strings. This package was created
in the spirit of
bytestring-show.

At the moment, text-show provides instances for most data
types in the array,
base,
bytestring, and
text packages.
Therefore, much of the source code for text-show consists of
borrowed code from those packages in order to ensure that the
behaviors of Show and TextShow coincide.

For most uses, simply importing TextShow
will suffice:


module Main where

import TextShow

main :: IO ()
main = printT (Just \"Hello, World!\")


If you desire it, there are also monomorphic versions of the showb
function available in the submodules of TextShow. See the
naming conventions
page for more information.

Support for automatically deriving TextShow instances can be found
in the TextShow.TH and TextShow.Generic modules.
"
HOMEPAGE="https://github.com/RyanGlScott/text-show"

LICENCES="BSD-3"
PLATFORMS="~amd64"

DEPENDENCIES="
    $(haskell_lib_dependencies "
        dev-haskell/array[>=0.3&<0.6]
        dev-haskell/base-compat[>=0.8.1&<1]
        dev-haskell/bifunctors[~>5.1]
        dev-haskell/bytestring[>=0.9&<0.11]
        dev-haskell/bytestring-builder
        dev-haskell/containers[>=0.1&<0.6]
        dev-haskell/contravariant[>=0.5&<2]
        dev-haskell/generic-deriving[~>1.11]
        dev-haskell/ghc-boot-th
        dev-haskell/integer-gmp
        dev-haskell/nats[>=0.1&<2]
        dev-haskell/semigroups[~>0.17]
        dev-haskell/tagged[>=0.4.4&<1]
        dev-haskell/template-haskell[=2.11*]
        dev-haskell/text[>=0.11.1&<1.3]
        dev-haskell/th-lift[>=0.7.6&<1]
        dev-haskell/transformers[=0.5*]
        dev-haskell/transformers-compat[~>0.5]
        dev-haskell/void[~>0.5]
    ")
    $(haskell_test_dependencies "
        dev-haskell/QuickCheck[~>2.5]
        dev-haskell/array[>=0.3&<0.6]
        dev-haskell/base-compat[>=0.8.2&<1]
        dev-haskell/base-orphans[>=0.5.2&<1]
        dev-haskell/bifunctors[~>5.1]
        dev-haskell/bytestring[>=0.9&<0.11]
        dev-haskell/bytestring-builder
        dev-haskell/containers[>=0.1&<0.6]
        dev-haskell/contravariant[>=0.5&<2]
        dev-haskell/deriving-compat[~>0.3]
        dev-haskell/generic-deriving[~>1.11]
        dev-haskell/ghc-boot-th
        dev-haskell/hspec[=2*]
        dev-haskell/integer-gmp
        dev-haskell/nats[>=0.1&<2]
        dev-haskell/quickcheck-instances[>=0.1&<0.4]
        dev-haskell/semigroups[~>0.17]
        dev-haskell/tagged[>=0.8.3&<1]
        dev-haskell/template-haskell[=2.11*]
        dev-haskell/text[>=0.11.1&<1.3]
        dev-haskell/th-lift[>=0.7.6&<1]
        dev-haskell/transformers[=0.5*]
        dev-haskell/transformers-compat[~>0.5]
        dev-haskell/void[~>0.5]
    ")
"

